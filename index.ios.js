import React, { Component } from 'react';
import MapView from 'react-native-maps';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  RefreshControl,
  ScrollView,
  TextInput,
  Keyboard,
  AsyncStorage,
  ListView
} from 'react-native';
import PageControl from 'react-native-page-control';
import City from './src/Components/City';
import AddCity from './src/Components/AddCity'

const screen = require('Dimensions').get('window');
const days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

export default class Weather extends Component {

  constructor(props) {
    super(props);
    this.addPage = this.addPage.bind(this);
    this.state =
    {
      currentPage:0,
      pageCount:0,
      pagesArray: [{city:"",location:true,order:0}],
      refreshing: false
    };
  }

  componentWillMount() {
    //AsyncStorage.setItem("pageList","");
    this.pageControl();
  }

  pageControl(){
    AsyncStorage.getItem("pageList").then(req=>JSON.parse(req)).then(json => {
      if(json!=null){
        this.setState({pagesArray:json});
        this.setState({pageCount: (this.state.pagesArray.length)});
      }
      else{
        AsyncStorage.setItem("pageList",JSON.stringify(this.state.pagesArray));
        this.setState({pageCount: (this.state.pagesArray.length)});
      }
    });
  }

  addPage  = () => {
    AsyncStorage.getItem("TextInput").then((text) => {
      AsyncStorage.getItem("pageList").then(req => JSON.parse(req)).then(json => {
        json.push({city:text,location:false,order:json.length});
        AsyncStorage.setItem("pageList",JSON.stringify(json));
        this.pageControl();
      });});
    }

    _onRefresh() {
      this.setState({refreshing: true});
      fetchData().then(() => {
        this.setState({refreshing: false});
      });
    }

    render() {

      renderPage = () =>{

        const _pagesArray = this.state.pagesArray.map((news) => {
          return(
            <City key={news.order} city={news.city} location={news.location}/>
          )
        });
        return  [_pagesArray, this.state.pageCount < 6 ? <AddCity addPage={this.addPage} textInput={this.props.textInput} key={0} /> : null]
      }

      return (
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />}>
            <View style={styles.container}>
              <Image
                style={styles.backgroundImage}
                source={require('./src/img/background.png')}
              />

              <ScrollView
                pagingEnabled
                horizontal
                showsHorizontalScrollIndicator={true}
                bounces={true}
                onScroll={this.onScroll}
                scrollEventThrottle={16}
                >
                  {renderPage()}
                </ScrollView>

                <PageControl style={{position:'absolute', left:0, right:0, bottom:50}}
                  numberOfPages={this.state.pageCount}
                  currentPage={this.state.currentPage}
                  hidesForSinglePage={true}
                  pageIndicatorTintColor='rgba(115,183,255,0.5)'
                  currentPageIndicatorTintColor='rgb(115,183,255)'
                  indicatorStyle={{borderRadius: 5}}
                  currentIndicatorStyle={{borderRadius: 5}}
                  indicatorSize={{width:8, height:8}}
                  onPageIndicatorPress={this.onItemTap} />
                </View>
              </ScrollView>
            );
          }

          onScroll = (event) => {
            var offsetX = event.nativeEvent.contentOffset.x,
            pageWidth = screen.width - 10;
            this.setState({currentPage:Math.floor((offsetX - pageWidth / 2) / pageWidth) + 1})
          }

        }

        const styles = StyleSheet.create({

          container: {
            flex: 1,
            flexDirection:'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#F5FCFF',
          },
          backgroundImage: {
            position:'absolute',
            height:screen.height,
            width:screen.width,
          },
          infoContainer:{
            flex:1,
            alignItems:'center',
            justifyContent:'flex-start',
            marginTop:75
          },
          tempText:{
            fontSize:125,
            fontWeight:'bold',
            fontFamily:'LemonMilk',
            color:'white',
            backgroundColor:'transparent',
          },
          tempSubText:{
            fontSize:60,
            lineHeight:125,
          },
          locationText:{
            fontSize:30,
            fontFamily:'SFUIDisplay-Medium',
            textAlign:'center',
            color:'white',
            backgroundColor:'transparent',
            width:screen.width,
            height:null
          },
          todayText:{
            fontSize:18,
            fontFamily:'SFUIDisplay-Light',
            color:'white',
            backgroundColor:'transparent',
            marginTop:5,
            opacity:0.75,
          },
          downPanel:{
            flex:1,
            marginTop:50
          },
          statusImage:{
            height:100,
            width:210,

          },
          windText:{
            color:'white',
            backgroundColor:'transparent',
            textAlign:'center',
            fontSize:25,
            fontFamily:'SFUIDisplay-Light',
            opacity:0.75,
            marginTop:10
          },
          page:{
            flex:1,
            flexDirection:'column',
            justifyContent:'center',
            alignItems:'center',
            width:screen.width,
            height:screen.height,
            backgroundColor:'transparent',
          },
        });

        AppRegistry.registerComponent('Weather', () => Weather);
