import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  AsyncStorage,
  Image,
  TouchableOpacity,
  TextInput
} from 'react-native';
const screen = require('Dimensions').get('window');
import index from '../../index.ios';

export default class AddCity extends Component {
  constructor(props) {
    super(props);
    this.state =
    {textInput: 'City Name'};
  }

  render() {
    return (
      <View style={styles.page}>
        <TextInput
          style={styles.locationText}
          onChangeText={(text) => {
            this.setState({textInput:text});
            AsyncStorage.setItem("TextInput",text);
          }}
          value={this.state.textInput}
          maxLength={10}
          >
          </TextInput>

          <TouchableOpacity onPress ={this.props.addPage}>
            <Image
              source={require('../img/add.png')}
              style={styles.addButton}
            />
          </TouchableOpacity>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    page:{
      flex:1,
      flexDirection:'column',
      justifyContent:'center',
      alignItems:'center',
      width:screen.width,
      height:screen.height,
      backgroundColor:'transparent',
    },
    addButton:{
      resizeMode:'contain',
      height:50,
      width:50
    },
    locationText:{
      fontSize:30,
      fontFamily:'SFUIDisplay-Medium',
      textAlign:'center',
      color:'white',
      backgroundColor:'transparent',
      width:screen.width,
      height:null,
      marginBottom:50
    },
  });
