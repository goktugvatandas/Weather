import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  Keyboard,
  AsyncStorage,
  Button,
  RefreshControl,
  ListView
} from 'react-native';
import images from '../img/WeatherStatus/index';

const screen = require('Dimensions').get('window');
const days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

export default class Weather extends Component {

  constructor(props) {
    super(props);
    this.state =
    {city: '',
    temp: '',
    location:'',
    windSpeed:'',
    humidity:'',
    sunrise:'',
    sunset:'',
    weathertype:'',
    currentDate:'',
    headerImage:require('../img/WeatherStatus/01d.png'),
  };
}

render() {
  return (
        <View style={styles.page}>
          <View style={styles.infoContainer}>
            <Text style={styles.tempText}>
              {this.state.temp}
              <Text style={styles.tempSubText}>
                °
              </Text>
            </Text>

            <Text style={styles.locationText}>
              {this.state.location}
            </Text>

            <Text style={styles.todayText}>
              {this.state.currentDate}
            </Text>
          </View>

          <View style={styles.downPanel}>
            <Image
              style={styles.statusImage}
              source={this.state.headerImage}
            />
            <Text style={styles.windText}>
              {this.state.windSpeed}
            </Text>
          </View>
        </View>
      );
    }

    componentWillMount(){

      const date = new Date();
      this.props.location ? this.getWeatherWithCurrentLocation() : this.getWeatherWithCityID(this.props.city)
      this.setState({currentDate:days[date.getDay()] +", "+ date.toLocaleTimeString()});
    }

    getWeatherWithCurrentLocation = () => {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          fetch('http://api.openweathermap.org/data/2.5/weather?lat='+position.coords.latitude+'&lon='+position.coords.longitude+'&appid=13ab49a3373514481b163e5e2153ac4a')
          .then(response => response.json())
          .then((responseData) =>{
            this.responseDataParse(responseData);
            this.setState({headerImage: images('_'+responseData.weather[0].icon)})
          })
          .catch((error) => {
            console.warn("Not found");
          });
        });
        this.setState({refreshing: false});
      }

      getWeatherWithCityID = (value) =>{
        Keyboard.dismiss();
        fetch('http://api.openweathermap.org/data/2.5/weather?q='+value+'&appid=13ab49a3373514481b163e5e2153ac4a')
        .then(response => response.json())
        .then((responseData) =>{
          this.responseDataParse(responseData);
          this.setState({headerImage: images('_'+responseData.weather[0].icon)});
        })
        .catch((error) => {
          console.warn(error);
        });
      }

      responseDataParse(responseData){

        this.setState({location:responseData.name});
        this.setState({temp:(responseData.main.temp-273.15).toFixed(0)});
        this.setState({windSpeed:responseData.wind.speed+" km/s"});
        this.setState({humidity:responseData.main.humidity+"%"});
        this.setState({weathertype:responseData.weather[0].description.toUpperCase()});

        var sunriseDate : Date = new Date((responseData.sys.sunrise * 1000));
        var sunsetDate : Date = new Date((responseData.sys.sunset * 1000));

        this.setState({sunrise:sunriseDate.toLocaleTimeString()});
        this.setState({sunset:sunsetDate.toLocaleTimeString()});

      }
    }

    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
      },
      infoContainer:{
        flex:1,
        alignItems:'center',
        justifyContent:'flex-start',
        marginTop:75
      },
      tempText:{
        fontSize:125,
        fontWeight:'bold',
        fontFamily:'LemonMilk',
        color:'white',
        backgroundColor:'transparent',
        marginLeft:40
      },
      tempSubText:{
        fontSize:60,
        lineHeight:125,
      },
      locationText:{
        fontSize:30,
        fontFamily:'SFUIDisplay-Medium',
        textAlign:'center',
        color:'white',
        backgroundColor:'transparent',
        width:screen.width,
        height:null
      },
      todayText:{
        fontSize:18,
        fontFamily:'SFUIDisplay-Light',
        color:'white',
        backgroundColor:'transparent',
        marginTop:5,
        opacity:0.75,
      },
      downPanel:{
        flex:1,
        marginTop:50
      },
      statusImage:{
        height:120,
        width:230,
        resizeMode: 'contain',
      },
      windText:{
        color:'white',
        backgroundColor:'transparent',
        textAlign:'center',
        fontSize:25,
        fontFamily:'SFUIDisplay-Light',
        opacity:0.75,
        marginTop:10
      },
      page:{
        flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        width:screen.width,
        height:screen.height,
        backgroundColor:'transparent',
      },
    });
