const images = (imageName) => {
 const obj = {
  _01d: require('./01d.png'),
  _02d: require('./02d.png'),
  _02n: require('./02n.png'),
  _03d: require('./03d.png'),
  _04d: require('./04d.png'),
  _09d: require('./09d.png'),
  _10d: require('./10d.png'),
  _11n: require('./11n.png'),
  _13d: require('./13d.png'),
  _13n: require('./13n.png'),
  _50d: require('./50d.png'),
  _50n: require('./50n.png'),
}
  return obj[imageName];

}

export default images;
